# SSL Certificates Kubernetes ingress trust SSL


## Self-Signed Certificates

### Generate root CA without any passphrase

1. Generate RSA root key
```bash
openssl genrsa -out CA-ROOT.key  4096
```
2. Generate a public root CA Cert
```bash
openssl req -new -x509 -sha256 -days 365 -key CA-ROOT.key -out CA-ROOT.crt
```
![alt text](https://raw.githubusercontent.com/bhagatdharmendra/Cka-exam/master/image_1.png)


## Verify Certificates
`openssl verify -CAfile ca.pem -verbose cert.pem`

## Install the CA Cert as a trusted root CA

### On Debian & Derivatives
- Move the CA certificate (`ca.pem`) into `/usr/local/share/ca-certificates/ca.crt`.
- Update the Cert Store with:
```bash
sudo update-ca-certificates
```

Refer the documentation [here](https://wiki.debian.org/Self-Signed_Certificate) and [here.](https://manpages.debian.org/buster/ca-certificates/update-ca-certificates.8.en.html)

### On Fedora
- Move the CA certificate (`ca.pem`) to `/etc/pki/ca-trust/source/anchors/ca.pem` or `/usr/share/pki/ca-trust-source/anchors/ca.pem`
- Now run (with sudo if necessary):
```bash
update-ca-trust
```

Refer the documentation [here.](https://docs.fedoraproject.org/en-US/quick-docs/using-shared-system-certificates/)
### On Arch
System-wide – Arch(p11-kit)
(From arch wiki)
- Run (As root)
```bash
trust anchor --store myCA.crt
```
- The certificate will be written to /etc/ca-certificates/trust-source/myCA.p11-kit and the "legacy" directories automatically updated.
- If you get "no configured writable location" or a similar error, import the CA manually:
- Copy the certificate to the /etc/ca-certificates/trust-source/anchors directory.
- and then
```bash 
update-ca-trust
```
![alt text](https://raw.githubusercontent.com/bhagatdharmendra/Cka-exam/master/image_2.png)
wiki page  [here](https://wiki.archlinux.org/title/User:Grawity/Adding_a_trusted_CA_certificate)

### On Windows

Assuming the path to your generated CA certificate as `C:\ca.pem`, run:
```powershell
Import-Certificate -FilePath "C:\ca.pem" -CertStoreLocation Cert:\LocalMachine\Root
```
- Set `-CertStoreLocation` to `Cert:\CurrentUser\Root` in case you want to trust certificates only for the logged in user.

OR

In Command Prompt, run:
```sh
certutil.exe -addstore root C:\ca.pem
```

- `certutil.exe` is a built-in tool (classic `System32` one) and adds a system-wide trust anchor.

### On Android

The exact steps vary device-to-device, but here is a generalised guide:
1. Open Phone Settings
2. Locate `Encryption and Credentials` section. It is generally found under `Settings > Security > Encryption and Credentials`
3. Choose `Install a certificate`
4. Choose `CA Certificate`
5. Locate the certificate file `ca.pem` on your SD Card/Internal Storage using the file manager.
6. Select to load it.
7. Done!


## Kubernetes staring point

### Secrets create for cluster issuer
* Need to create base64 encoded for both CA key and CA crt.
```bash
cat CA-ROOT.crt | base64 -w0;echo
cat CA-ROOT.key | base64 -w0;echo
```
* Create secret from above encoded value
```bash
apiVersion: v1
kind: Secret
metadata:
  name: ca-key-pair
  namespace: cert-manager
data:
 #"encoded vaules of CA crt"
 tls.crt: 
 #"encoded vaules of CA key"
 tls.key:
```
 ### ClusterIssuer create for cert-manager
 ```bash
apiVersion: cert-manager.io/v1
kind: ClusterIssuer
metadata:
  name: ca-internal-selfcert-clusterissuer
  
spec:
  ca:
    secretName: ca-key-pair
 ```
  ```bash
kubectl apply -f secret.yaml
 ```
 ### Certificate create and bound with ClusterIssuer
   ```bash
kubectl create namespace test1
 ```
 ```bash
apiVersion: cert-manager.io/v1
kind: Certificate
metadata:
  name: example-com
  namespace: test1
spec:
  # Secret names are always required.
  secretName: example-com-tls
  secretTemplate:
    annotations:
      my-secret-annotation-1: "foo"
      my-secret-annotation-2: "bar"
    labels:
      my-secret-label: foo

  duration: 2160h # 90d
  renewBefore: 360h # 15d
  subject:
    organizations:
      - jetstack
  commonName: example.com
  isCA: false
  privateKey:
    algorithm: RSA
    encoding: PKCS1
    size: 2048
  usages:
    - server auth
    - client auth
  # At least one of a DNS Name, URI, or IP address is required.
  dnsNames:
    - example.com
    - www.example.com
  ipAddresses:
    - 192.168.0.5
  # Issuer references are always required.
  issuerRef:
    name: ca-internal-selfcert-clusterissuer
    kind: ClusterIssuer
    group: cert-manager.io
 ```
 ```bash
kubectl apply -f Certificate.yaml
 ```

 ### Simple app deployment,svc & ingress
```bash
---
apiVersion: apps/v1
kind: Deployment
metadata:
  creationTimestamp: null
  labels:
    app: webserver
  name: webserver
  namespace: test1
spec:
  replicas: 2
  selector:
    matchLabels:
      app: webserver
  strategy: {}
  template:
    metadata:
      creationTimestamp: null
      labels:
        app: webserver
    spec:
      containers:
      - image: bhagatdharmendra0/webserver-php-ip
        name: webserver-php-ip
        resources: {}
status: {}
---

apiVersion: v1
kind: Service
metadata:
  creationTimestamp: null
  labels:
    app: webserver
  name: webserver
  namespace: test1
spec:
  ports:
  - port: 80
    protocol: TCP
    targetPort: 80
  selector:
    app: webserver
  type: ClusterIP
status:
  loadBalancer: {}
```
```bash
kubectl apply -f simple-deploy-svc.yaml
```
#### finally deploy ingress with SSL 
```bash
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: ingress-resource-2
  namespace: test1
spec:
  ingressClassName: nginx
  rules:
  - host: www.example.com
    http:
      paths:
      - path: /
        pathType: Prefix
        backend:
          service:
            name: webserver
            port: 
              number: 80
  tls:
  - hosts:
    - www.example.com
    secretName: example-com-tls
```
```bash
kubectl apply -f ingress.yaml
```
#### Before access the application using name DNS records should be preset or create entry as per mentioned below

![alt text](https://raw.githubusercontent.com/bhagatdharmendra/Cka-exam/master/image-4.png)







