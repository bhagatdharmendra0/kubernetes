
# Kubernetes cluster provisioning using Vagrant

The Kubernetes cluster can be setup on your local machine with one master node and two worker nodes. 



## Prerequisite
1. VirtualBox & Vagrant should be installed on your system. They are easy to install.
2. It is required to have 4 cores and 4 gigs of RAM





## Installation

For Installation kubernetes cluster follow the below steps.

```bash
  git clone https://gitlab.com/bhagatdharmendra0/kubernetes.git
  cd kubernetes/Vagrant
  vagrant up
```
Installing my K8s cluster should take around ten minutes, depending on your computer and internet speed. For all kubernetes nodes root password is redhat.
    
## K8s-cluster information

| Nodes             | IPs                                                                |
| ----------------- | ------------------------------------------------------------------ |
| k8s-master | 172.16.11.100 |
| k8s-worker1 | 172.16.11.101 |
| k8s-worker2 | 172.16.11.102 |



## Post cluster build  

The kubeconfig file needs to be copied into your local user's ~/.kube/config folder from master server in order to access the kubernetes cluster. root password is redhat
```bash
 mkdir ~/.kube
 scp root@172.16.11.100:/etc/kubernetes/admin.conf ~/.kube/config
 kubectl get nodes -o wide  
```
As a result, your cluster is now up and running. You can now begin working on it and enjoy it :)
## Stop and Destroy cluster
Stop the cluster ; you all work will be there on cluster
```bash
vagrant halt
```
The cluster has been destroyed; all the work is gone. This means that next time you will get a new cluster when you run vagrant up command.
```bash
vagrant destroy -f
```
When you destroy your cluster, if you want a complete clean, you also need to delete newly created network interfaces. You should make sure the IP address of the newly created interface matches the IP address of the destroyed cluster(172.16.11.X).
```bash
vboxmanage hostonlyif remove "VirtualBox Host-Only Ethernet Adapter #4"
```




## 🚀 About Me
I am a DevOps engineer and you can find out more about me on LinkedIn. 

[![linkedin](https://img.shields.io/badge/linkedin-0A66C2?style=for-the-badge&logo=linkedin&logoColor=white)](https://www.linkedin.com/in/dharmendra-bhagat-374282146/)

