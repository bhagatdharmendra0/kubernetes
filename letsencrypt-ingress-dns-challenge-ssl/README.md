# kubernetes GoDaddy DNS challenge *wildcard certificate

Deploying true producation ssl for application in kubernetes cluster eks.

## Prerequisite for the EKS cluster build
1. awscli,eksctl & kubectl download and install as per your Operating system 
awscli
```bash
 https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html
```
#### Most Importatantly godaddy webhook
We are following this github repository ```https://github.com/snowdrop/godaddy-webhook.git```
 

eksctl
```bash
https://docs.aws.amazon.com/eks/latest/userguide/eksctl.html
```
kubectl 
```bash
https://kubernetes.io/docs/tasks/tools/
```
GIT 
```bash
https://git-scm.com/downloads
```
After installations we need to configure awscli run below command and provide access key and secret key
```bash
aws configure
```
### Creating EKS cluster on aws
2. set ENV variable for kubeconfig file if you don't want to marge with you default config file ~/.kube/config
```bash
export KUBECONFIG=~/.kube/eks-config
```
2. Creating EKS cluster on aws
```bash
eksctl create cluster --name nginx-cluster --version 1.25 --region ap-south-1 \
--nodegroup-name k8s-nodes \
--node-type t2.medium \
--nodes 2
```


### Deploy nginx ingress controller on eks cluster
```bash
helm repo add nginx-stable https://helm.nginx.com/stable
helm repo update
helm install nginx-ingress nginx-stable/nginx-ingress -n ingress-nginx --set rbac.create=true --create-namespace
```

### Deploy Cert-Manager on EKS cluster for certificate management
1. installations of cert-manager on cert-manager namespace
```bash
kubectl apply -f https://github.com/jetstack/cert-manager/releases/download/v1.13.0/cert-manager.yaml
```



## We need to configure GoDaddy api accesskey & secret key 
##### So that cert-manager able to connect via api godaddy got to ``` https://developer.godaddy.com/``` then create keys choose type ``prod``


![alt text](https://raw.githubusercontent.com/bhagatdharmendra/Cka-exam/refs/heads/master/DNS%20CHAll/godaddy-dev-pag.png)

![alt text](https://raw.githubusercontent.com/bhagatdharmendra/Cka-exam/master/DNS%20CHAll/access-key.png)



## Clone the repository
```bash 
git clone https://github.com/snowdrop/godaddy-webhook.git
helm install -n cert-manager godaddy-webhook ./godaddy-webhook/deploy/charts/godaddy-webhook
kubectl apply -f godaddy-webhook/deploy/webhook-all.yml --validate=false

```

### Deploying secret for cert-manager 
so that cert-manager can comunicate with GoDaddy using api

```bash
apiVersion: v1
kind: Secret
metadata:
  name: godaddy-api-key
  namespace: cert-manager
type: Opaque
stringData:
  token: gHKpM1pj45d3_3TqLXnZatd3GSyPRpxVaQb:TY4iJnif8pHVExUhPjjpzM #accesskey:secretkey both is maaped
```
```bash
kubectl apply -f secret.yaml
```
### Deploying cluster issuer

```bash
apiVersion: cert-manager.io/v1
kind: ClusterIssuer
metadata:
  name: letsencrypt-prod
spec:
  acme:
    # ACME Server
    # prod : https://acme-v02.api.letsencrypt.org/directory
    # staging : https://acme-staging-v02.api.letsencrypt.org/directory
    server: https://acme-v02.api.letsencrypt.org/directory 
    # ACME Email address
    email: bhagat.aws@gmail.com
    privateKeySecretRef:
      name: letsencrypt-prod # staging or production
    solvers:
    - selector:
        dnsNames:
        - '*.bhagatgroups.com'
      dns01:
        webhook:
          config:
            apiKeySecretRef:
              name: godaddy-api-key
              key: token
            production: true
            ttl: 600
          groupName: acme.mycompany.com
          solverName: godaddy
```
```bash
kubectl apply -f clusterissuer.yml
```
### Deploying certificate

```bash
apiVersion: cert-manager.io/v1
kind: Certificate
metadata:
  name: wildcard-bhagatgroups-com
spec:
  secretName: wildcard-bhagatgroups-com-tls
  renewBefore: 240h
  dnsNames:
  - '*.bhagatgroups.com'
  issuerRef:
    name: letsencrypt-prod
    kind: ClusterIssuer
```
```bash
kubectl apply -f certificate.yaml
```
**After certificate creation, on the fly TXT records created for DNS challenge as mention below**

![alt text](https://raw.githubusercontent.com/bhagatdharmendra/Cka-exam/master/DNS%20CHAll/Auto-TXT-created.png)
## Deploying simple application and exposing using nginx ingress controller

```bash
git clone https://gitlab.com/bhagatdharmendra0/kubernetes.git
cd kubernetes/letsencrypt-ingress-dns-challenge-ssl/ingress/
kubectl apply -f deploy.yaml
kubectl apply -f svc.yaml
kubectl apply -f ingress.yaml
```
### Completed - Now attach certificate with ingress resource
1. vertify if certificate if you get or not. Run below command and ready should be **True** if everything is works fine
![alt text](https://raw.githubusercontent.com/bhagatdharmendra/Cka-exam/master/DNS%20CHAll/certificate.png)
2. Now mention the secret name is ingress as showing below

![alt text](https://raw.githubusercontent.com/bhagatdharmendra/Cka-exam/master/DNS%20CHAll/ksnip_ingress.png)

3. **Finally try to access application using https**
```bash
https://redhat.bhagatgroups.com/
```
![alt text](https://raw.githubusercontent.com/bhagatdharmendra/Cka-exam/master/DNS%20CHAll/webpage-access.png)

4. **Lets test the wildcard certificate - deploy one more application and expose using same TLS secret**
```bash
kubectl create deployment test2 --image=httpd --replicas 1 --port=80 -n cert-manager
kubectl expose -n cert-manager deployment  test2 --type=ClusterIP --port=80 
kubectl apply test2-ingress.yaml
```
![alt text](https://raw.githubusercontent.com/bhagatdharmendra/Cka-exam/master/DNS%20CHAll/both-records.png)

![alt text](https://raw.githubusercontent.com/bhagatdharmendra/Cka-exam/master/DNS%20CHAll/webpage-access-2.png)


## After all testing destory the EKS cluster
```bash
eksctl delete cluster --name nginx-cluster --region ap-south-1
```
#### delete the GoDaddy secret as well.