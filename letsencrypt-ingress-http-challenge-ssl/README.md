# kubernetes nginx ingress cert-manager letsencrypt SSL 

Deploying true producation ssl for application in kubernetes cluster eks.

## Prerequisite for the EKS cluster build
1. awscli,eksctl & kubectl download and install as per your Operating system 
awscli
```bash
 https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html
```
eksctl
```bash
https://docs.aws.amazon.com/eks/latest/userguide/eksctl.html
```
kubectl 
```bash
https://kubernetes.io/docs/tasks/tools/
```
GIT 
```bash
https://git-scm.com/downloads
```
After installations we need to configure awscli run below command and provide access key and secret key
```bash
aws configure
```
### Creating EKS cluster on aws
2. set ENV variable for kubeconfig file if you don't want to marge with you default config file ~/.kube/config
```bash
export KUBECONFIG=~/.kube/eks-config
```
2. Creating EKS cluster on aws
```bash
eksctl create cluster --name nginx-cluster --version 1.25 --region ap-south-1 \
--nodegroup-name k8s-nodes \
--node-type t2.micro \
--nodes 2
```


### Deploy nginx ingress controller on eks cluster
```bash
helm repo add nginx-stable https://helm.nginx.com/stable
helm repo update
helm install nginx-ingress nginx-stable/nginx-ingress -n ingress-nginx --set rbac.create=true --create-namespace
```

### Deploy Cert-Manager on EKS cluster for certificate management
1. installations of cert-manager on cert-manager namespace
```bash
kubectl apply -f https://github.com/cert-manager/cert-manager/releases/download/v1.12.0/cert-manager.yaml
```



## Deploying simple application and exposing using nginx ingress controller

```bash
git clone https://gitlab.com/bhagatdharmendra0/kubernetes.git
kubectl apply -f kubernetes/letsencrypt-nginx-ingress-ssl/ingress/deploy.yaml
kubectl apply -f kubernetes/letsencrypt-nginx-ingress-ssl/ingress/svc.yaml
kubectl apply -f kubernetes/letsencrypt-nginx-ingress-ssl/ingress/ingress-withoutssl.yaml
```

## create DNS records for letsencrypt ACME challenge process
1. It will take time to visiable the DNS changes to end user it might take 1-2 hours.
![alt text](https://raw.githubusercontent.com/bhagatdharmendra/Cka-exam/master/ksnip_20230912-234216.png)

2. Tyep to access application using name in our case
```bash
http://test.bhagatgroups.com/
````
3. Once application is access using DNS name then only process next.


### Deploying SSL on EKS cluster for ingress nginx

```bash
kubectl apply -f kubernetes/letsencrypt-nginx-ingress-ssl/clusterissuer.yaml
kubectl apply -f kubernetes/letsencrypt-nginx-ingress-ssl/certificate.yaml
```
### Completed - Now attach certificate with ingress resource
1. vertify if certificate if you get or not. Run below command and ready should be **True** if everything is works fine
![alt text](https://raw.githubusercontent.com/bhagatdharmendra/Cka-exam/master/ksnip_20230913-000346.png)
2. Now mention the secret name is ingress as showing below.
![alt text](https://raw.githubusercontent.com/bhagatdharmendra/Cka-exam/master/ingress.png)
3. **Finally try to access application using https**
```bash
https://test.bhagatgroups.com/
```

## After all testing destory the EKS cluster
```bash
eksctl delete cluster --name nginx-cluster --region ap-south-1
```