
# Metallb deploy on kubernetes on-premises cluster

The on-premises Kubernetes cluster does not have the capability to expose applications using the loadbalancer service type. As a result, you can expose applications using metallb using the service type loadbalancer. Once the new loadbalancer is created, metallb will assign IP addresses to those loadbalancers based on the IP pool address range defined in metallb.  
## K8s-cluster information

| Nodes             | IPs                                                                |
| ----------------- | ------------------------------------------------------------------ |
| k8s-master | 172.16.11.100 |
| k8s-worker1 | 172.16.11.101 |
| k8s-worker2 | 172.16.11.102 |




## Metallb Deployment on kubernetes

In-order to installtion the metallb you need to run this command. you can check out metallb website for more information [Metallb website](https://metallb.universe.tf/).



```bash
  kubectl apply -f https://raw.githubusercontent.com/metallb/metallb/v0.13.9/config/manifests/metallb-native.yaml
```




## L2Advertisement & IPAddressPool assign to metallb



we need to first create IPAddressPool where we mentioned from which IP or subnet loadbalcer will used. In my case I have choose last few nodes IPs 172.16.11.30-45.

```bash
git clone https://gitlab.com/bhagatdharmendra0/kubernetes.git
cd kubernetes/metallb
kubectl apply -f IPAddressPool.yaml
```
After create IPAddressPool we need to bound with kubernetes cluster for that we need to create L2Advertisement.
```bash
kubectl apply -f L2Advertisement.yaml
```







## Test the service type LoadBalancer 

Install my-project with npm

```bash
  kubectl run nginx --image=nginx --port=80
  kubectl expose pods nginx --type=LoadBalancer
  kubectl get svc nginx
```
Just open any web bowser and type the external ip address which you able to see when you run get svc command.  
## 🚀 About Me
I am a DevOps engineer and you can find out more about me on LinkedIn. 

[![linkedin](https://img.shields.io/badge/linkedin-0A66C2?style=for-the-badge&logo=linkedin&logoColor=white)](https://www.linkedin.com/in/dharmendra-bhagat-374282146/)