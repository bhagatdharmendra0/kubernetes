# Monitoring stack kubernetes

#### Deploying Prometheus Operator and prometheus kube-stack chart

```bash 
helm repo add prometheus-community https://prometheus-community.github.io/helm-charts

helm  install prometheus --values Helm-charts/values-kube-prometheus-stack.yaml prometheus-community/kube-prometheus-stack -n monitoring --create-namespace
```
Default etcd nodes is showing down because servicemonitor object not able to reach to matrics need to create below secrets
```bash 
kubectl create secret generic etcd-certs -n monitoring --from-file=ca.crt=ca.crt --from-file=client.crt=apiserver-etcd-client.crt --from-file=client.key=apiserver-etcd-client.key

```
![alt text](images/image.png)

```bash 
kubectl port-forward svc/prometheus-prometheus 9090

```
#### Deploying Grafana Operator

```bash
helm repo add grafana https://grafana.github.io/helm-charts 
helm install grafana bitnami/grafana-operator --version 3.8.3 -n monitoring --values Helm-charts/values-grafan-operator.yaml
``` 
![alt text](images/image2.png)

![alt text](images/image3.png)

```bash
kubectl port-forward svc/grafana-grafana-operator-grafana-service 3000
```
```bash
kubectl apply -f Grafan-crds/autorization-grafan.yaml
kubectl apply -f Grafan-crds/grafana-folders.yaml
kubectl apply -f Grafan-crds/grafana-datasource-promethes.yaml
kubectl apply -f Grafan-crds/grafana-DashBoard.yaml
```

## Create Own Alertrules & prometheusrules to trigger alerts