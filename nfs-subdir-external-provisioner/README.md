
# NFS-subdir-external-provisioner as storageclass in k8s
NFS subdir external provisioner is an automatic provisioner that use your existing and already configured NFS server to support dynamic provisioning of Kubernetes Persistent Volumes via Persistent Volume Claims. - [NFS-subdir-external-provisioner](https://github.com/kubernetes-sigs/nfs-subdir-external-provisioner)

## K8s-cluster & NFS server information
In-order to setup the nfs-subdir-external as storage class we need nfs server in my case I have setup master nodes as nfs server as well.
| Nodes | IPS     | NFS info                |
| :-------- | :------- | :------------------------- |
| k8s-master | 172.16.11.100 | NFS-server|
| k8s-worker1 | 172.16.11.101 | NFS-client|
| k8s-worker2 | 172.16.11.102 | NFS-client|



## NFS server and Client setup & testing

* NFS-server server setup -In my case all kubernetes cluster is hosted on top of ubuntu 22.04 that's why I have mentioned below how to setup nfs server on ubuntu OS.

```bash
  ssh root@172.16.11.100
  apt-get install nfs-kernel-server -y
  mkdir -p /mnt/nfs-storageclass
  chown -R nobody:nogroup /mnt/nfs-storageclass
  echo "/mnt/nfs-storageclass *(rw,sync,no_subtree_check,no_root_squash,no_all_squash,insecure)" >> /etc/exports
  exportfs -rvf
  systemctl restart nfs-kernel-server
```
* NFS Client side setup:- need to perform on all the worker nodes(k8s-worker1	& k8s-worker2) :- `ssh root@k8s-worker1` then  `ssh root@k8s-worker2`
   ```bash
    apt install nfs-common -y
    showmount -e 172.16.11.100
    mount -t nfs 172.16.11.100:/mnt/nfs-storageclass /mnt
    df -ThP /mnt
    umount /mnt
   ```

## Prerequisites
Make sure you have installed helm on your work station from where you run kubectl command.
* Helm - [Download & Install helm](https://github.com/helm/helm/releases) post installtion you must get output after running below helm command.

```bash
helm version --short 

```
## Deployment of NFS-subdir-external-provisioner using Helm

1. First need to add helm repo and then extract the helm chart value so that we can able to modify as per our requirement.
```bash
helm repo add nfs-subdir-external-provisioner https://kubernetes-sigs.github.io/nfs-subdir-external-provisioner
helm repo list
helm show values nfs-subdir-external-provisioner/nfs-subdir-external-provisioner >nfs-helm-value.yaml
```
2. Changing values on the extracted helm values file [nfs-helm-value.yaml](https://gitlab.com/bhagatdharmendra0/kubernetes/-/blame/dev-nfssubdir/nfs-subdir-external-provisioner/nfs-helm-value.yaml) after that deploy on the kubernetes cluster.
```
sed -i 's/accessModes: ReadWriteOnce/accessModes: ReadWriteMany/g' nfs-helm-value.yaml
sed -i 's/name: nfs-client/name: nfs-k8s-masterclient/g' nfs-helm-value.yaml
 vim nfs-helm-value.yaml 
  # Storage class annotations
  annotations: 
    "storageclass.kubernetes.io/is-default-class": "true"
```
3. Finally after changing values now Deployment on the kubernetes cluster in any namespace in my case nfs-storageclass.
```bash
helm install nfs-k8s-masterclient nfs-subdir-external-provisioner/nfs-subdir-external-provisioner \
--values nfs-helm-value.yaml -n nfs-storageclass --create-namespace  --set nfs.server=172.16.11.100 \
--set nfs.path=/mnt/nfs-storageclass
```
4. Verify :- All pods should be in running state on `nfs-storageclass` namespace.
```bash
helm list -n nfs-storageclass
kubectl get all -n nfs-storageclass
```




## Test the newly create storage class
There are two files in simple-pvc1.yaml and simple-pvc2.yaml, however on the PV2 file, there is no storage class listed as in the above steps. On the Helm value chart, we added the annotations default class to the helm value chart. As a result, when you create a pvc without specifying any storage class, this class will be used by default. Must importantly all the PVC should be in `bound` state.
```bash
git clone https://gitlab.com/bhagatdharmendra0/kubernetes.git
cd kubernetes/nfs-subdir-external-provisioner/
kubectl apply -f simple-pvc1.yaml
kubectl apply -f simple-pvc2.yaml
kubectl get pvc
```



##  About Me
I am a DevOps engineer and you can find out more about me on LinkedIn. 

[![linkedin](https://img.shields.io/badge/linkedin-0A66C2?style=for-the-badge&logo=linkedin&logoColor=white)](https://www.linkedin.com/in/dharmendra-bhagat-374282146/)



