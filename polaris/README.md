
# Polaris enforce policies to kubernetes

Polaris is an open source policy engine for Kubernetes that validates and remediates resource configuration. It includes 30+ built in configuration policies, as well as the ability to build custom policies with JSON Schema. When run on the command line or as a mutating webhook, Polaris can automatically remediate issues based on policy criteria.

![Alt text](https://raw.githubusercontent.com/bhagatdharmendra/Cka-exam/Image/polaris/polaris-web.png)



## Prerequisite
1.cert-manager should be up and running


 


## Installation of cert-manager

For Installation kubernetes cluster follow the below steps.

```bash
 helm repo add jetstack https://charts.jetstack.io
helm install \
  cert-manager jetstack/cert-manager \
  --namespace cert-manager \
  --create-namespace \
  --version v1.13.2 \
  --set installCRDs=true
  --set prometheus.enabled=false \  
  --set webhook.timeoutSeconds=4   

```
## Installation of polaris
The time of installing custom values files has passed, on the values.yaml we mentioned some denger arguments, such as runasroot, in that case Polaris won't deploy those deployments that didn't follow best practices(danger).
```bash 
helm repo add fairwinds-stable https://charts.fairwinds.com/stable
helm install polaris fairwinds-stable/polaris --namespace polaris --create-namespace \
--values values.yaml --set webhook.enable=true --set dashboard.enable=true
```

## Demo
![Alt text](https://raw.githubusercontent.com/bhagatdharmendra/Cka-exam/Image/polaris/polaris-deny.png)



## 🚀 About Me
I am a DevOps engineer and you can find out more about me on LinkedIn. 

[![linkedin](https://img.shields.io/badge/linkedin-0A66C2?style=for-the-badge&logo=linkedin&logoColor=white)](https://www.linkedin.com/in/dharmendra-bhagat-374282146/)

